#include <game/scene/node.h>

#ifndef FRAME_H
#define FRAME_H

struct Node* make_frame(void);
void free_frame(struct Node* frame);

#endif
