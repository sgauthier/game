#ifndef GAME_INIT_H
#define GAME_INIT_H

int game_init(const char* gameShaderRootPath);
void game_free(void);

#endif
